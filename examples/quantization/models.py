import torch.nn as nn
import snntorch as snn
import brevitas.nn as qnn

def build_default_model(num_inputs, num_hidden, num_outputs, beta):
    net = nn.Sequential(
            nn.Linear(num_inputs, num_hidden),
            snn.Leaky(beta=beta, init_hidden=True),
            nn.Linear(num_hidden, num_outputs),
            snn.Leaky(beta=beta, init_hidden=True, output=True),
          )
    return net

# TODO: replace Linear layers with qnn.QuantLinear
def build_quantized_model(num_inputs, num_hidden, num_outputs, beta):
    net = nn.Sequential(
            nn.Linear(num_inputs, num_hidden),
            snn.Leaky(beta=beta, init_hidden=True),
            nn.Linear(num_hidden, num_outputs),
            snn.Leaky(beta=beta, init_hidden=True, output=True),
          )
    return net
