import torch
import torch.nn as nn
import snntorch
from snntorch import spikegen
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
from spinnaker2 import snn, hardware
import snntorch_to_spinnaker2.converter as converter
import numpy as np
import matplotlib.pyplot as plt

# dataloader arguments
batch_size = 1
data_path='./data/mnist'

dtype = torch.float
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
num_steps = 25

# Define a transform
transform = transforms.Compose([
            transforms.Resize((28, 28)),
            transforms.Grayscale(),
            transforms.ToTensor(),
            transforms.Normalize((0,), (1,))])

mnist_train = datasets.MNIST(data_path, train=True, download=True, transform=transform)
mnist_test = datasets.MNIST(data_path, train=False, download=True, transform=transform)

# Create DataLoaders
train_loader = DataLoader(mnist_train, batch_size=batch_size, shuffle=True, drop_last=True)
test_loader = DataLoader(mnist_test, batch_size=batch_size, shuffle=False, drop_last=True)
iterator = iter(test_loader)
test_data, test_targets = next(iterator)
test_data, test_targets = next(iterator)
test_data, test_targets = next(iterator)
test_spike_data_raw = spikegen.rate(test_data, num_steps=num_steps)

test_spike_data = test_spike_data_raw.view(num_steps, -1).T
input_spikes = {}
for i,row in enumerate(test_spike_data):
    input_spikes[i] = np.where(row == 1)[0]

# reduce number of neurons per core
MAX_LIF_NEURON_PER_CORE = 10

# Define Network
class Net(nn.Module):
    def __init__(self):
        super().__init__()

        # Initialize layers
        self.fc1 = nn.Linear(num_inputs, num_hidden)
        self.lif1 = snntorch.Leaky(beta=beta)
        self.fc2 = nn.Linear(num_hidden, num_outputs)
        self.lif2 = snntorch.Leaky(beta=beta)

    def forward(self, x):
        # Initialize hidden states at t=0
        mem1 = self.lif1.init_leaky()
        mem2 = self.lif2.init_leaky()
        # Record the final layer
        spk2_rec = []
        mem2_rec = []

        for step in range(num_steps):
            cur1 = self.fc1(x[step])
            spk1, mem1 = self.lif1(cur1, mem1)
            cur2 = self.fc2(spk1)
            spk2, mem2 = self.lif2(cur2, mem2)
            spk2_rec.append(spk2)
            mem2_rec.append(mem2)

        return torch.stack(spk2_rec, dim=0), torch.stack(mem2_rec, dim=0)

path = './model_save.pth'
# - load the whole model
model = torch.load(path)

_, snntorch_output = model(test_spike_data_raw.view(num_steps, batch_size, -1))
plt.figure()
for i, vs in enumerate(torch.squeeze(snntorch_output).detach().numpy().T):
    plt.plot(vs, label=f"Neuron {i}") 
plt.legend()
plt.title("snnTorch")

print(model)
# - access parameters
print(model.state_dict().keys())
print(model.state_dict().get('lif1.threshold'))
print(model.fc1)

# layers = list(model.children())

input_layer = snn.Population(model.fc1.in_features, "spike_list", input_spikes)

# fc1
weights_scaled, bias_scaled, scale = converter.convert_linear(model.fc1,127)
# lif1
lif1_params = converter.convert_leaky(model.lif1)
lif1_params["i_offset"] = bias_scaled
lif1_params["threshold"] = lif1_params["threshold"]*scale

lif1_pop = snn.Population(model.fc1.out_features, "lif_no_delay", lif1_params, "lif1", record=['spikes'])
lif1_pop.set_max_atoms_per_core(MAX_LIF_NEURON_PER_CORE)
print(model.fc1.out_features)
print(type(model.fc1.out_features))
print(lif1_params)

conns1 = converter.connection_list_from_dense_weights(weights_scaled.T)
proj1 = snn.Projection(input_layer, lif1_pop, conns1)

# fc2
weights_scaled, bias_scaled, scale = converter.convert_linear(model.fc2,127)
# lif1
lif2_params = converter.convert_leaky(model.lif2)
lif2_params["i_offset"] = bias_scaled
lif2_params["threshold"] = lif2_params["threshold"]*scale

lif2_pop = snn.Population(model.fc2.out_features, "lif_no_delay", lif2_params, "lif2", record=['v','spikes'])
lif2_pop.set_max_atoms_per_core(MAX_LIF_NEURON_PER_CORE)

conns2 = converter.connection_list_from_dense_weights(weights_scaled.T)
proj2 = snn.Projection(lif1_pop, lif2_pop, conns2)

net = snn.Network("snntorch network")
net.add(input_layer, lif1_pop, lif2_pop, proj1, proj2)

# hw = hardware.SpiNNaker2Chip() # use slow JTAG connection
hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.59") # use faster ethernet
timesteps = num_steps
hw.run(net, timesteps, mapping_only=False, debug=False, sys_tick_in_s=100.e-3)

# get results and plot

print("expected digit:", test_targets)
#spikes = pop1.get_spikes()[0]
times = range(num_steps)
voltages = lif2_pop.get_voltages()
plt.figure()
for neuron, vs in voltages.items():
    plt.plot(times, vs, label=f"Neuron {neuron}") 
plt.xlim(0,timesteps)
plt.xlabel("time step")
plt.ylabel("voltage")
plt.legend()
plt.title("SpiNNaker2")
plt.show()
