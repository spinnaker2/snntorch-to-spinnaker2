import torch, torch.nn as nn
import snntorch as st
from snntorch import surrogate, utils
from spinnaker2 import hardware
import snntorch_to_spinnaker2.sequential as sq
import snntorch_to_spinnaker2.converter as con


num_steps = 25 # number of time steps
batch_size = 1
in_size = 100
hidden_size = 50
out_size = 10

alpha = 0.8
beta = 0.8
threshold = 0.5
spike_grad = surrogate.fast_sigmoid() # surrogate gradient

net = nn.Sequential(
      nn.Linear(in_size, hidden_size),
      st.Leaky(beta=beta, threshold=threshold, init_hidden=True, spike_grad=spike_grad),
      nn.Linear(hidden_size, hidden_size),
      st.Synaptic(alpha=alpha, beta=beta, threshold=threshold, init_hidden=True, spike_grad=spike_grad),
      nn.Linear(hidden_size, hidden_size),
      st.Leaky(beta=beta, threshold=threshold, init_hidden=True, spike_grad=spike_grad),
      nn.Linear(hidden_size, out_size),
      st.Synaptic(alpha=alpha, beta=beta, threshold=threshold, init_hidden=True, spike_grad=spike_grad, output=True)
      )

utils.reset(net) # reset/initialize hidden states for all neurons

# random input data with 25% probability for spikes
data_in = (torch.rand(num_steps, batch_size, 100) < 0.25).float()
spike_recording = [] # record spikes over time

for step in range(num_steps): # loop over time
    output = net(data_in[step]) # one time step of forward-pass
    
    if isinstance(output, tuple) and len(output) >= 2:
        spike, state = output[:2]  # Unpack the first two values if available
    else:
        spike = output  # Use the output as spike if no state is returned

    spike_recording.append(spike) # record spikes in list

s2_net, in_pop, out_pop = sq.convert_sequential(net,10)

# convert spiketrain and set it for input population
s2_spikes = con.convert_input_spikes(spike_recording)
in_pop.params = s2_spikes

# run on s2 hardware:
hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.59")
hw.run(s2_net, num_steps)

# get results
spikes = out_pop.get_spikes()
voltages = out_pop.get_voltages()
print(spikes)
print(voltages)
