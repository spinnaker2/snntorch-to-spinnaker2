import torch.nn as nn
import snntorch as st
import snntorch_to_spinnaker2.sequential as sq
import pytest
from collections import OrderedDict

# test to check conversion of sequential model 
def test_normal_convert_sequential():
    net = nn.Sequential(
        nn.Linear(50, 30),
        st.Leaky(beta=0.5, init_hidden=True),
        nn.Linear(30, 30),
        st.Leaky(beta=0.7,  init_hidden=True),
        nn.Linear(30, 30),
        st.Leaky(beta=0.8, init_hidden=True),
        nn.Linear(30, 10),
        st.Synaptic(alpha=0.3, beta=0.4, init_hidden=True, output=True)
        )
    try:
        s2_net, in_pop, out_pop = sq.convert_sequential(net,10)
    
    except Exception as e:
        pytest.fail(f"An unexpected exception was raised: {e}")

# test for missing Linear between two Synaptic layers
def test_missing_linear_raises_exception():
    
    net1 = nn.Sequential(
        nn.Linear(100, 50),
        st.Leaky(beta=0.7, threshold=0.5, init_hidden=True),
        nn.Linear(50, 50),
        st.Synaptic(alpha=0.3, beta=0.6, init_hidden=True),
        st.Synaptic(alpha=0.6, beta=0.4, init_hidden=True),
        nn.Linear(50, 15),
        st.Leaky(beta=0.7, init_hidden=True, output=True),
        )
    
    with pytest.raises(Exception):
        s2_net, in_pop, out_pop = sq.convert_sequential(net1,10)
    
    # test for missing Linear at start
    net2 = nn.Sequential(
        st.Leaky(beta=0.7, init_hidden=True),
        nn.Linear(100, 50),
        st.Leaky(beta=0.7, init_hidden=True),
        nn.Linear(50, 50),
        st.Synaptic(alpha=0.3, beta=0.6, init_hidden=True),
        st.Synaptic(alpha=0.6, beta=0.4, init_hidden=True),
        nn.Linear(50, 15),
        st.Leaky(beta=0.7, init_hidden=True, output=True),
        )
    
    with pytest.raises(Exception):
        s2_net, in_pop, out_pop = sq.convert_sequential(net2,10)

# test for exceptions other than linear, synaptic and leaky
def test_different_neuron_exception():
    
    net = nn.Sequential(
        nn.Linear(50, 30),
        st.Leaky(beta=0.5, init_hidden=True),
        nn.Linear(30, 30),
        st.RLeaky(beta=0.7, all_to_all=False, init_hidden=True),
        nn.Linear(30, 30),
        st.Leaky(beta=0.8, init_hidden=True),
        nn.Linear(30, 10),
        st.Synaptic(alpha=0.3, beta=0.4, init_hidden=True, output=True)
        )

    with pytest.raises(Exception):
        s2_net, in_pop, out_pop = sq.convert_sequential(net,10) 

# test for ordered dictionary
def test_ordered_dict():

    net = nn.Sequential(OrderedDict([
        ('linear1',nn.Linear(100, 50)),
        ('leaky1',st.Leaky(beta=0.8, init_hidden=True)),
        ('leaky1',st.Leaky(beta=0.8, init_hidden=True)),
        ('linear2',nn.Linear(50, 50)),
        ('synaptic1',st.Synaptic(alpha=0.8, beta=0.8, init_hidden=True)),
        ('linear3',nn.Linear(50,50)),
        ('leaky2',st.Leaky(beta=0.8, init_hidden=True, output=True)),
        ]))
    
    try:
        s2_net, in_pop, out_pop = sq.convert_sequential(net,10)
    
    except Exception as e:
        pytest.fail(f"An unexpected exception was raised: {e}")

# test for ordered dictionary exception 
def test_ordered_dict_exception():

    net = nn.Sequential(OrderedDict([
        ('linear1',nn.Linear(100, 80)),
        ('leaky1',st.Leaky(beta=0.5, init_hidden=True)),
        ('linear2',nn.Linear(80,80)),
        ('synaptic1',st.Synaptic(alpha=0.6, beta=0.5, init_hidden=True)),
        ('synaptic3',st.Synaptic(alpha=0.6, beta=0.5, init_hidden=True)),
        ('linear3',nn.Linear(80,80)),
        ('leaky2',st.Leaky(beta=0.5, init_hidden=True)),
        ('linear4',nn.Linear(80, 30)),
        ('synaptic2',st.Synaptic(alpha=0.6, beta=0.5, init_hidden=True, output=True))
        ]))
    
    with pytest.raises(Exception):
        s2_net, in_pop, out_pop = sq.convert_sequential(net,10)
        
if __name__ == "__main__":
    test_normal_convert_sequential()
    test_missing_linear_raises_exception()
    test_ordered_dict()
    test_different_neuron_exception()
    test_ordered_dict_exception()
