import snntorch_to_spinnaker2.converter as con
import matplotlib.pyplot as plt
import numpy as np
import math
import snntorch as snnt
import torch.nn 
from spinnaker2 import hardware, snn

def compare_leaky(neu_params = {}, weight=1, input_spike = [], number_steps = 0, do_plot=False, check_voltage=False, check_spike_count=False):

    lif1 = snnt.Leaky(beta=neu_params["beta"], threshold=neu_params["threshold"], reset_mechanism=neu_params["reset_method"])

    spk_in = torch.zeros(number_steps)

    for i in input_spike:
        spk_in[i] = weight
    
    mem = torch.zeros(1)
    spk = torch.zeros(1)
    mem_rec = []
    spk_rec = []

    for step in range(number_steps):
        spk, mem = lif1(spk_in[step], mem)
        spk_rec.append(spk)
        mem_rec.append(mem)

    spk_rec = torch.stack(spk_rec)
    mem_rec = torch.stack(mem_rec)

    Syn_con_param = con.convert_leaky(lif1)

    pop1 = snn.Population(
        size=1,
        neuron_model="lif_no_delay",
        params=Syn_con_param,
        name="pop1", record=['spikes', 'v'])
    
    stim = snn.Population(1,"spike_list", {0:input_spike}, "stim")

    delay=0
    conns = [[0,0, weight, delay]]
    proj = snn.Projection(stim, pop1, conns)

    net = snn.Network("my network")
    net.add(stim,pop1,proj)

    hw = hardware.SpiNNaker2Chip(eth_ip="192.168.1.59")
    timesteps = number_steps
    hw.run(net, timesteps)

    spikes = pop1.get_spikes()[0]
    voltages = pop1.get_voltages()

    spk_arr = spk_rec.detach().numpy()
    snn_spikes = np.asarray(spk_arr==1).nonzero()
    mem_arr = mem_rec.detach().numpy().flatten()

    if do_plot:
        times = np.arange(timesteps)
        plt.plot(times, voltages[0], color='r',label="spinnaker 2")
        plt.plot(mem_rec.detach().numpy(), color='g', label="snn torch") 
        plt.axhline(y=neu_params["threshold"], alpha=0.25, linestyle="dashed", c="black", linewidth=2)
        plt.xlim(0,timesteps)
        plt.xlabel("time step")
        plt.ylabel("voltage")
        plt.legend()
        plt.show()
    
    if check_voltage:
        assert np.allclose(voltages[0][1:],mem_arr[:-1], atol = 1e-6)

    if check_spike_count:
        assert math.isclose(len(spikes),len(snn_spikes[0]),abs_tol=2)

def test_convert_leaky_negative_weight(do_plot=False):
    param = {"beta" : 0.6, "threshold" : 1 , "reset_method" : "zero"}
    input = [10,30,40,60,70,80,90,100,120,130,150,160,190]
    compare_leaky(param,-1,input,200,do_plot,check_voltage=True)

def test_convert_leaky_sub_threshold(do_plot=False):
    param = {"beta" : 0.7, "threshold" : 3 , "reset_method" : "subtract"}
    input = [40,60,70,80,100,110,120, 130, 140, 150, 160, 170]
    compare_leaky(param,1,input,200,do_plot,check_voltage=True)

def test_convert_leaky_reset_by_subtraction(do_plot=False):
    param = {"beta" : 0.8, "threshold" : 2.1 , "reset_method" : "subtract"}
    input = [0,10,20,30,40,50,60,70,80,90,100,110,120, 130, 140, 150, 160, 170, 180, 190]
    compare_leaky(param,2,input,200,do_plot,check_spike_count=True)

def test_convert_leaky_reset_to_zero(do_plot=False):    
    param1 = {"beta" : 0.9, "threshold" : 4 , "reset_method" : "zero"}
    input1 = [0,10,20,30,40,50,60,70,80,90,100,110,120, 130, 140, 150, 160, 170, 180, 190]
    compare_leaky(param1,3,input1,200,do_plot,check_spike_count=True)

if __name__ == "__main__":
    test_convert_leaky_sub_threshold()
    test_convert_leaky_negative_weight()
    test_convert_leaky_reset_by_subtraction()
    test_convert_leaky_reset_to_zero()