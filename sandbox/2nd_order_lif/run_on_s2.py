"""
Example for LIF neuron with exponentially decaying current input
"""
import numpy as np
import matplotlib.pyplot as plt

from spinnaker2 import snn, hardware 

# Temporal dynamics
syn_decay = 0.9
mem_v_decay= 0.8
num_steps = 200
w = 0.2
w_scaled = 2
threshold = 1.*w_scaled/w

input_spikes = np.arange(0,num_steps,10, dtype=np.uint32)

params = {
        "threshold":threshold,
        "alpha_decay":mem_v_decay,
        "i_offset":0.0,
        "v_reset":0.,
        "reset":"reset_by_subtraction",
        "exc_decay":syn_decay,
        "inh_decay":syn_decay,
        "t_refrac":0,
        }

pop1 = snn.Population(
        size=1,
        neuron_model="lif_curr_exp",
        params=params,
        name="pop1", record=['spikes', 'v'])

stim = snn.Population(1,"spike_list", {0:input_spikes}, "stim")

delay = 0
conns = [[0,0, w_scaled, delay]]
proj = snn.Projection(stim, pop1, conns)

net = snn.Network("my network")
net.add(stim,pop1,proj)

hw = hardware.SpiNNaker2Chip()
timesteps = num_steps
hw.run(net, timesteps)

# get results and plot

spikes = pop1.get_spikes()[0]
voltages = pop1.get_voltages()
times = np.arange(timesteps)
plt.plot(times, voltages[0], label="Neuron 0") 
plt.xlim(0,timesteps)
plt.xlabel("time step")
plt.ylabel("voltage")
plt.legend()
plt.show()
