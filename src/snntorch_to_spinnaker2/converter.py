from typing import Tuple

import numpy as np
import snntorch as snn
import torch
import torch.nn as nn


def get_reset_method_s2(reset_mechanism):
    """
    TODO: add support for mechanism `none`
    """
    if reset_mechanism == "subtract":
        return "reset_by_subtraction"
    elif reset_mechanism == "zero":
        return "reset_to_v_reset"
    else:
        raise Exception("Unsupported reset mechanism")


def convert_linear(
    layer: nn.Linear, max_s2_weight: int = 127, s2_weight_type: np.dtype = np.int8
) -> Tuple[np.ndarray, np.ndarray, float]:
    """Converts parameters of a Linear layer to scaled parameters suitable for
    py-spinnaker2.

    Synaptic weights in py-spinnaker2 are integers in the range [-127, 127].
    This functions extracts the weights and biases from a torch.nn.Linear layer
    as numpy arrays and scales them to the dynamic range available in
    py-spinnaker2. A scaling factor is obtained such that the maximum absolute
    weight of the layer is scaled to `max_s2_weight`. The weights and biases
    are scaled by this factor. The weights are further converted to the
    datatype specified in `s2_weight_type`.

    Args:
        layer: the Linear layer.
        max_s2_weight: maximum absolute weight in py-spinnaker2.
        s2_weight_type: datatype to which the scaled weights shall be converted.

    Returns:
        Tuple with 3 elements:
            (scaled_weights, scaled_biases, scaling_factor)
    """
    weights = layer.weight.detach().numpy()
    bias = layer.bias.detach().numpy()
    max_abs_weight = np.abs(weights).max()
    scale = max_s2_weight / max_abs_weight
    weights_scaled = weights * scale
    bias_scaled = bias * scale

    return weights_scaled.astype(s2_weight_type), bias_scaled, scale


def convert_leaky(layer: snn.Leaky):
    """
    extracts parameters of a snntorch.Leaky layer to parameters for the
    SpiNNaker2 `lif` model.
    """
    v_mem_decay = layer.beta.numpy().tolist()
    threshold = layer.threshold.numpy().tolist()
    reset = layer.reset_mechanism
    reset_s2 = get_reset_method_s2(reset)

    params = {
        "threshold": threshold,
        "alpha_decay": float(v_mem_decay),  # FIXME
        "reset": reset_s2,
    }
    return params


def convert_synaptic(layer: snn.Synaptic):
    """
    extracts parameters of a snntorch.Synaptic layer to parameters for the
    SpiNNaker2 `lif_curr_exp` model.
    """
    v_mem_decay = layer.beta.numpy().tolist()
    syn_decay = layer.alpha.numpy().tolist()
    threshold = layer.threshold.numpy().tolist()
    reset = layer.reset_mechanism
    reset_s2 = get_reset_method_s2(reset)

    params = {
        "threshold": threshold,
        "alpha_decay": v_mem_decay,
        "reset": reset_s2,
        "exc_decay": syn_decay,
        "inh_decay": syn_decay,
    }
    return params


def connection_list_from_dense_weights(dense_weights, delay=0):
    conns = []
    for i in range(dense_weights.shape[0]):
        for j in range(dense_weights.shape[1]):
            conns.append([i, j, dense_weights[i, j], delay])
    return conns


def convert_input_spikes(input_data):
    """
    Converts input spikes from a snn torch into a format that is compatible 
    with the PySpinnaker2 framework.
    """
    spikes_dict = {}
    for step, spikes in enumerate(input_data):
        spikes_dict[step] = spikes.detach().numpy().nonzero()[1]
    return spikes_dict
