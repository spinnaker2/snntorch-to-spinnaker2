from snntorch_to_spinnaker2 import converter
import torch.nn as nn
import snntorch as st
from spinnaker2 import snn

def convert_sequential(model: nn.Sequential, max_neurons_per_core: int):
    """Convert snnTorch Sequential model to SpiNNaker2 network.

    Supported modules:
        - torch.nn.Linear
        - snntorch.Leaky
        - snntorch.Synaptic

    Args:
        model: the sequential model
        max_neurons_per_core: the maximum number of atoms per core, value cannot be larger than the maximum number of atoms supported by the ARM application.

    Returns:
        tuple with 3 elements: (network, input_pop, output_pop)

        Details:
            network (spinnaker2.snn.Network): the SpiNNaker2 network object
            input_pop (spinnaker2.snn.Population): the input population of the network, needed to set input spikes
            ouput_pop (spinnaker2.snn.Population): the output population of the network, needed for getting the results after simulation 
    """
    
    network = snn.Network("snntorch network")
    input_pop = None    # input population
    prev_pop = None       
    output_pop = None   # output population

    for i in range(len(model)):

        # Linear layer
        if isinstance(model[i],nn.Linear):
            if i==0:
                # create input population for first linear layer
                input_pop = snn.Population(model[i].in_features, "spike_list", params={})
                network.add(input_pop)
                prev_pop = input_pop      
            else:
                continue    # proceeding to the next linear layer if it's not the first one 
        
        # Leaky layer
        elif isinstance(model[i],st.Leaky):
            
            # checking whether previous layer is Linear or not
            if isinstance(model[i-1],nn.Linear):
                weights_scaled, bias_scaled, scale = converter.convert_linear(model[i-1],127)
            else:
                raise Exception("Previous layer is not Linear layer")
            
            lif_l_params = converter.convert_leaky(model[i])
            lif_l_params["i_offset"] = bias_scaled
            lif_l_params["threshold"] = lif_l_params["threshold"]*scale

            lif_l_pop = snn.Population(model[i-1].out_features, "lif_no_delay", lif_l_params, f"lif_{i}", record=['v', 'spikes'])
            lif_l_pop.set_max_atoms_per_core(max_neurons_per_core)

            conns = converter.connection_list_from_dense_weights(weights_scaled.T)
            proj = snn.Projection(prev_pop, lif_l_pop, conns)
            network.add(lif_l_pop,proj)
            prev_pop = lif_l_pop
            if i==len(model)-1:
                output_pop = lif_l_pop    # assign population to output, if its the last layer

        # Synaptic layer
        elif isinstance(model[i],st.Synaptic):
            
            # checking whether previous layer is Linear or not
            if isinstance(model[i-1],nn.Linear):
                weights_scaled, bias_scaled, scale = converter.convert_linear(model[i-1],127)
            else:
                raise Exception("Previous layer is not Linear layer")

            lif_s_params = converter.convert_synaptic(model[i])
            lif_s_params["i_offset"] = bias_scaled
            lif_s_params["threshold"] = lif_s_params["threshold"]*scale

            lif_s_pop = snn.Population(model[i-1].out_features, "lif_curr_exp_no_delay", lif_s_params, f"lif_curr_exp_{i}", record=['v', 'spikes'])
            lif_s_pop.set_max_atoms_per_core(max_neurons_per_core)

            conns = converter.connection_list_from_dense_weights(weights_scaled.T)
            proj = snn.Projection(prev_pop, lif_s_pop, conns)
            network.add(lif_s_pop, proj)
            prev_pop = lif_s_pop
            if i==len(model)-1:
                output_pop = lif_s_pop    # assign population to output, if its the last layer

        else:
            raise Exception("Supports only Linear, Leaky and Synaptic layers")

    return network, input_pop, output_pop
