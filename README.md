# snntorch-to-spinnaker2

Tools and examples for running spiking neural networks (SNN) trained with
[snnTorch](https://github.com/jeshraghian/snntorch) on the SpiNNaker2 hardware
using [py-spinnaker2](https://gitlab.com/spinnaker2/py-spinnaker2).

## Installation

Please install the following python packages as described in their documentation:
- [snnTorch](https://github.com/jeshraghian/snntorch)
- [torchvision](https://pypi.org/project/torchvision/)
- [chardet](https://pypi.org/project/chardet/)
- [py-spinnaker2](https://gitlab.com/spinnaker2/py-spinnaker2) with the low-level code for the SpiNNaker2 chip

## Usage

### Example of feedforward network with snntorch.Leaky neurons for MNIST
1. Train model in snnTorch:
```
python snntorch_leaky_training.py`
```

2. Run on SpiNNaker2
```
python spinnaker2_inference.py
```

### Example of single 2nd order LIF neuron (snntorch.Synaptic)

See [sandbox/2nd_order_lif](sandbox/2nd_order_lif)

## Support
Please contact Bernhard Vogginger (bernhard.vogginger@tu-dresden.de)

## Contributing
Contributions are very welcome!

## Authors and acknowledgment
This project was started during the [2022 Telluride Neuromorphic Cognition Engineering Workshop](https://sites.google.com/view/telluride-2022/home).
Initial contributors: [Bernhard Vogginger](https://gitlab.com/bvogginger) and [Peng Zhou](https://gitlab.com/pengzhouzp).

## License
See [LICENSE](LICENSE).

## Project status
While there is no progress since the Telluride 2022, TU Dresden plans to
continue this activity to easily support SNNs trained in snnTorch on
SpiNNaker2.
